exports.config = {
		
		seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
		framework:'custom',
		baseURL: 'http://localhost:8080/',
		specs : ['Features/*.feature'],
	    capabilities:{
	    	
	    	browserName: 'chrome',
	    	chromeOptions:{ args:['start-maximized']}
	    },
	    
	    frameworkPath: require.resolve('protractor-cucumber-framework'),
	    cucumberOpts:{
	    	
	    	require: ['Step-Definations/steps.js']
	    }
	   		
}