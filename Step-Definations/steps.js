var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect; 

const { Given, When, Then } = require('cucumber');


Given('I open the website URL' , {timeout: 80 * 1000} , function(){
	
	return browser.driver.get("https://codverter.com/");
	
});

When('I land on the home page',function(){
	
	const title = browser.getTitle().then(function(){
		//Verify whether I have landed in the Main page of the website by verifying the title
    return expect(title).to.eventually.equal("CodVerter");
	
	})
});

Then('Navigate to Login Page',{timeout : 5000},function(){
	
	    //Click On Login Link
	    element(by.id("b9")).click().then(function(){
			
		//Verify User is Navigated to login page from home page
		var ExpectedPageName = "Log In";
		var LoadedPageName = element(by.tagName("h2")).getText();
		console.log(LoadedPageName);
		
		return expect(LoadedPageName).to.eventually.equal(ExpectedPageName);
	
	})

});

//Verify whether user is able to login
Then("Login with Valid credentials",{timeout : 9000},function(){
	 
	 browser.sleep(5000);
	 element(by.id("txtemail")).sendKeys("test4@test.com");
	 element(by.id("txtpass")).sendKeys("Test@123"); 
	 return element(by.css("button[id='btnsubmit']")).click();
	
});

Then("Verify whether user is navigated to Account Information Page after login", {timeout : 30000} , function(){
	
	browser.sleep(8000);
	var ExpectedPageName = "Account Information";
	var LoadedPageName = element(by.tagName("h4")).getText();
	console.log(LoadedPageName);
	
	return expect(LoadedPageName).to.eventually.equal(ExpectedPageName);
});
	 
	
