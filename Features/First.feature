Feature: Validation of Codeverter Website

Scenario: Validation of Navigation from Home Page to Login Page and then to Account Information Page

	Given I open the website URL
	When I land on the home page
	Then Navigate to Login Page
	Then Login with Valid credentials
	Then Verify whether user is navigated to Account Information Page after login
	